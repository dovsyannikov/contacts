package com.example.contacts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactActivity extends AppCompatActivity {

    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String ADDRESS = "ADDRESS";
    public static final String PHONE = "PHONE";
    public static final String IMAGE = "IMAGE";

    @BindView(R.id.ci_txtName)
    TextView ci_txtName;
    @BindView(R.id.ci_txtPhone)
    TextView ci_txtPhone;
    @BindView(R.id.ci_txtAddress)
    TextView ci_txtAddress;
    @BindView(R.id.ci_txtEmail)
    TextView ci_txtEmail;
    @BindView(R.id.ci_contactImage)
    ImageView ci_contactImage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        ci_txtName.setText(String.format("Name: %s", intent.getStringExtra(NAME)));
        ci_txtPhone.setText(String.format("Phone: %s", intent.getStringExtra(PHONE)));
        ci_txtAddress.setText(String.format("Address: %s", intent.getStringExtra(ADDRESS)));
        ci_txtEmail.setText(String.format("Email: %s", intent.getStringExtra(EMAIL)));
        ci_contactImage.setImageResource(intent.getIntExtra(IMAGE, R.drawable.ic_launcher_background));


    }
}
