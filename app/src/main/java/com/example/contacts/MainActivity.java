package com.example.contacts;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.example.contacts.Adapters.MyAdapter;
import com.example.contacts.models.Contact;
import com.example.contacts.models.Generator;

import java.util.List;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity {

    public static final String NAME = "NAME";
    public static final String EMAIL = "EMAIL";
    public static final String ADDRESS = "ADDRESS";
    public static final String PHONE = "PHONE";
    public static final String IMAGE = "IMAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final List<Contact> list = Generator.generate();

        RecyclerView recyclerView = findViewById(R.id.contactView);
        MyAdapter adapter = new MyAdapter(this, list);
        recyclerView.setAdapter(adapter);
        adapter.setListener(new MyAdapter.OnItemClickListener() {
            @Override
            public void onClick(Contact contact) {
                Intent intent = new Intent(MainActivity.this, ContactActivity.class);
                intent.putExtra(NAME, contact.getName());
                intent.putExtra(EMAIL, contact.getEmail());
                intent.putExtra(ADDRESS, contact.getAddress());
                intent.putExtra(PHONE, contact.getPhone());
                intent.putExtra(IMAGE, contact.getImage());
                startActivity(intent);
            }
        });
    }
}
