package com.example.contacts.models;

public class Contact {
    private String name;
    private String email;
    private String phone;
    private String address;
    private int image;

    public Contact(String name, String email, String phone, String address, int image) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public int getImage() {
        return image;
    }
}
