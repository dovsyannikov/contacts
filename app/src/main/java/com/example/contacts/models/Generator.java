package com.example.contacts.models;

import com.example.contacts.R;
import com.example.contacts.models.Contact;

import java.util.ArrayList;
import java.util.List;

public class Generator {
    public static List<Contact> generate(){
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Gregory V. Gleason", "Gregory@rhyta.com", "260-482-9114", "2229 Windy Ridge Road\n" +
                "Fort Wayne, IN 46805", R.drawable.man));
        contacts.add(new Contact("Alice S. Leclair", "Alice@rhyta.com", "(03) 5373 0233", "5 Edmundsons Road\n" +
                "MOLLONGGHIP VIC 3352", R.drawable.woman));
        contacts.add(new Contact("Richard S. Risinger", "Richard@teleworm.us", "(03) 8631 1819", "27 Creedon Street\n" +
                "PARKVILLE VIC 3052", R.drawable.man));
        return contacts;
    }
}
